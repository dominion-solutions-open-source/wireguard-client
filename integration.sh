#!/usr/bin/env sh
POSITONAL_ARGS=""
BASEDIR="/tmp/ansible_collections"
WORKDIR="${BASEDIR}/dominion_solutions/wireguard_client"

show_help()
{
    HELP='Usage: integration.sh --help | --docker <container_version>\n
    Executes the integration tests using the supplied docker container.\n
    Args:\n
    -d, --docker: The docker container to run against.
    Must be one of the supported ones listed at: https://github.com/ansible/ansible/blob/devel/test/lib/ansible_test/_data/completion/docker.txt\n
    -h, --help: Show this help message.';
    echo $HELP;
    exit 0;
}

process_parameters()
{
    while [ $# -gt 0 ]
    do
        case "${1}" in
            -d|--docker)
                DOCKER="${2}"
                shift 2
            ;;
            -h|--help)
                show_help
            ;;
            *)
                POSITONAL_ARGS="${POSITONAL_ARGS} ${1}"
                shift
            ;;
        esac
    done
}

process_parameters $@
if [ -z $DOCKER ]
then
    show_help
fi
. ./.venv/bin/activate


pip install -r requirements.txt
mkdir -p $WORKDIR
cp -f -r . $WORKDIR

chdir $BASEDIR

ansible-galaxy collection install -r dominion_solutions/wireguard_client/tests/integration/targets/setup/meta/main.yml -p .

chdir $WORKDIR

ansible-test integration --docker ${DOCKER} ${POSITONAL_ARGS}
